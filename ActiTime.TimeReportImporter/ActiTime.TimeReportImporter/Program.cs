﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;

namespace ActiTime.TimeReportImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var currExecutionPath = $"{AppDomain.CurrentDomain.BaseDirectory}SeleniumDrivers";

            using (IWebDriver driver = new ChromeDriver(currExecutionPath))
            {
                var loginPage = new LoginPage(driver);
                PageFactory.InitElements(driver, loginPage);

                loginPage.GoTo("http://actitime.softiti.com/actitime/login.do");
                var homePage = loginPage.SignIn("maczer", "Altimi33");
                
                Console.ReadLine();
            }
        }
    }
}
