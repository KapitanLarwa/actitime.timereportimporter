﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;

namespace ActiTime.TimeReportImporter
{
   
    public class LoginPage
    {
        private readonly IWebDriver webDriver;

        [FindsBy(How = How.Name, Using = "pwd")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "loginButton")]
        public IWebElement SubmitButton { get; set; }

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement UserName { get; set; }

        public LoginPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void GoTo(string url)
        {
            if (!url.Contains("login"))
            {
                throw new StaleElementReferenceException("This is not a login page");
            }

            webDriver.Navigate().GoToUrl(url);
        }

        public HomePage SignIn(string username, string password)
        {
            UserName.SendKeys(username);
            Password.SendKeys(password);
            SubmitButton.Click();

            // Even if i create a NUnit test for this
            // Issue with page loading still occures when I try and return new object
            HomePage homePage = new HomePage(webDriver);
            PageFactory.InitElements(webDriver, homePage);
            return homePage;
        }
    }
}
