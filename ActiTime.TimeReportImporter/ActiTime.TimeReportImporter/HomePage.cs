﻿using OpenQA.Selenium;

namespace ActiTime.TimeReportImporter
{
    public class HomePage
    {
        private readonly IWebDriver _webDriver;

        public HomePage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
    }
}